﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {
	
	public GameObject enemyPrefab;
	public Transform target;

	public int spawnCount = 10;
	public float spawnTime = 2f;

	private float cooldown = 2f;

	void Start() {
		StartCoroutine(SpawnEnemy());
	}

	IEnumerator SpawnEnemy() {
		yield return new WaitForSeconds(cooldown);

		for (int i = 0; i < spawnCount; i++) {
			GameObject gameObject = Instantiate(enemyPrefab, transform);
			Enemy enemy = gameObject.GetComponent<Enemy>();
			enemy.target = target;

			yield return new WaitForSeconds(spawnTime);
		}
	}
}
