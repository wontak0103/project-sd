﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour {
	public Transform target;
	public float speed = 10f;

	public Image healthBar;
	public GameObject deathEffectPrefab;

	public float maxHealthPoint = 100f;

	private float healthPoint = 0f;

	void Awake() {
		healthPoint = maxHealthPoint;
	}
	
	void Update () {
		Vector3 direction = target.position - transform.position;
		transform.Translate(direction.normalized * speed * Time.deltaTime, Space.World);

		if (Vector3.Distance(target.position, transform.position) <= 0.2f) {
			Destroy(gameObject);
		}
	}

	public void TakeDamage(float damage) {
		healthPoint -= damage;

		UpdateHealthBar();

		if (healthPoint <= 0f)
			Death();
	}

	public void UpdateHealthBar() {
		float factor = healthPoint / maxHealthPoint;
		if (factor < 0f)
			factor = 0f;
		
		healthBar.fillAmount = factor;
	}

	public void Death() {
		GameObject effectGo = Instantiate(deathEffectPrefab, transform.position, transform.rotation);
		Destroy(effectGo, 2f);

		Destroy(gameObject);
	}
}
