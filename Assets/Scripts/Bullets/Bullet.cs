﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {
	public float damage = 10f;
	public float speed = 10f;

	public GameObject effectPrefab;

	protected virtual void Update() {
		transform.Translate(transform.forward.normalized * speed * Time.deltaTime, Space.World);
	}

	protected virtual void OnTriggerEnter(Collider collider)
	{
		Enemy enemy = collider.GetComponent<Enemy>();
		if (enemy != null) {
			enemy.TakeDamage(damage);
		}

		GameObject effectGo = Instantiate(effectPrefab, transform.position, transform.rotation);
		Destroy(effectGo, 2f);
		Destroy(gameObject);
	}
}
